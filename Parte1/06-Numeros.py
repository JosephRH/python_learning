numero = 2  # entero integer
decinal = 1.2  # float
imaginario = 2 + 2j  # 2 + 21

# numero = numero + 2
numero += 5
numero *= 5
numero -= 5
numero /= 5

print("numero", numero)
print(1 + 3)
print(1 - 3)
print(1 * 3)
print(1 / 3)
print(1 // 3)
print(8 % 3)
print(2 ** 3)
